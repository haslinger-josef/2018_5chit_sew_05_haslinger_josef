﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace _180928_MTM
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        public class Student
        {
            public Student()
            {
                this.Courses = new HashSet<Course>();
            }

            public int StudentId { get; set; }
            [Required]
            public string StudentName { get; set; }

            public virtual ICollection<Course> Courses { get; set; }
        }

        public class Course
        {
            public Course()
            {
                this.Students = new HashSet<Student>();
            }

            public int CourseId { get; set; }
            public string CourseName { get; set; }

            public virtual ICollection<Student> Students { get; set; }
        }

        public class SchoolDBContext : DbContext
        {

            public DbSet<Student> Students { get; set; }
            public DbSet<Course> Courses { get; set; }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);
            }
        }
    }
}
