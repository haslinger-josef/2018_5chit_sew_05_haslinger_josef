﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace _180928_OTM
{
    class Program
    {
        static void Main(string[] args)
        {
            

            using (InheritanceMappingContext ctx = new InheritanceMappingContext())
            {

                Student student = new Student
                {
                    StudentName = "Josef"
                };
                Student student2 = new Student
                {
                    StudentName = "Benjamin"
                };

                Grade grade1 = new Grade
                {
                    GradeName = "5CHIT",
                    Section = "HTL IT",
                    Students = new List<Student>(new Student[] { student, student2 })
                };

                ctx.Student.Add(student);
                ctx.Student.Add(student2);
                ctx.Grade.Add(grade1);
                ctx.SaveChanges();
            }

        }

        public class Student
        {
            public int StudentId { get; set; }
            public string StudentName { get; set; }
        }

        public class Grade
        {
            public int GradeId { get; set; }
            public string GradeName { get; set; }
            public string Section { get; set; }

            public ICollection<Student> Students { get; set; }
        }
        class InheritanceMappingContext : DbContext
        {
            public DbSet<Student> Student { get; set; }
            public DbSet<Grade> Grade { get; set; }
        }
    }
}
