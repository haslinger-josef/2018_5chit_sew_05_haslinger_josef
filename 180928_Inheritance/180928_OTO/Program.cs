﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace _180928_OTO
{
    class Program
    {
        static void Main(string[] args)
        {
            using (InheritanceMappingContext ctx = new InheritanceMappingContext())
            { 

                Student student = new Student
                {
                    StudentName = "Josef",
                    Address = new StudentAddress
                    {
                        Address1 = "Purrath 4"
                    }
                };
                ctx.Student.Add(student);
                ctx.SaveChanges();
            }
        }
        public class Student
        {
           

            public int StudentId { get; set; }
            public string StudentName { get; set; }

            public virtual StudentAddress Address { get; set; }
        }

        public class StudentAddress
        {
            [ForeignKey("Student")]
            public int StudentAddressId { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public int Zipcode { get; set; }
            public string State { get; set; }
            public string Country { get; set; }

            public virtual Student Student { get; set; }
        }
        class InheritanceMappingContext : DbContext
        {
            public DbSet<Student> Student { get; set; }
            public DbSet<StudentAddress> StudentAddress { get; set; }
        }

    }
}
