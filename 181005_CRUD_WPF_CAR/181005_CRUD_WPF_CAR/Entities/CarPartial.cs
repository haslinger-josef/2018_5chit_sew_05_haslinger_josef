using System.ComponentModel.DataAnnotations.Schema;

namespace _181005_CRUD_WPF_CAR
{ 
	public partial class Car
	{
		public override string ToString()
		{
			// Since the PetName column could be empty, supply
			// the default name of **No Name**.
			return $"{this.PetName ?? "**No Name**"} is a" +
                $" {this.Color} {this.Make} with ID {this.CarId}.";
		}

        [NotMapped]
	    public string MakeColor => $"{Make} + ({Color})";
	}
}