﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace _181005_CRUD_WPF_CAR
{
    class MyDataInitializer : DropCreateDatabaseIfModelChanges<CarsContext>
    {
        protected override void Seed(CarsContext context)
        {
            var customers = new List<Customer>
            {
                new Customer{FirstName="Dave",LastName="Brenner"},
                new Customer{FirstName="Matt",LastName="Walton"},
                new Customer{FirstName="Steve",LastName="Hagon"},
                new Customer{FirstName="Pat",LastName="Walton"},
                new Customer{FirstName="Bad",LastName="Customer"}
            };
            customers.ForEach(x => context.Customers.Add(x));

            var cars = new List<Car>
            {
                new Car{Make="VW",Color="Black",PetName="Zippy"},
                new Car{Make="Ford",Color="Rust",PetName="Rusty"},
                new Car{Make="Saab",Color="Black",PetName="Mel"},
                new Car{Make="Yugo",Color="Yellow",PetName="Clunker"},
                new Car{Make="BMW",Color="Black",PetName="Bimmer"},
                new Car{Make="BMW",Color="Green",PetName="Hank"},
                new Car{Make="BMW",Color="Pink",PetName="Pinky"},
                new Car{Make="Pinto",Color="Black",PetName="Pete"},
                new Car{Make="Yugo",Color="Brown",PetName="Brownie"}
            };
            cars.ForEach(x => context.Cars.Add(x));

            var orders = new List<Order>
            {
                new Order{Car=cars[0],Customer=customers[0]},
                new Order{Car=cars[1],Customer=customers[1]},
                new Order{Car=cars[2],Customer=customers[2]},
                new Order{Car=cars[3],Customer=customers[3]}
            };

            orders.ForEach(x => context.Orders.Add(x));

            context.CreditRisks.Add(
                new CreditRisk
                {
                    CustId = customers[4].CustId,
                    FirstName = customers[4].FirstName,
                    LastName = customers[4].LastName
                });

            context.SaveChanges();
        }
    }
}
