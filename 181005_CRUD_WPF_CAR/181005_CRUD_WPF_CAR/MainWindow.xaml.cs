﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _181005_CRUD_WPF_CAR
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CarRepository carRepo;
        DbSet<Car> cars;
        CarsContext context;
        public MainWindow()
        {
            InitializeComponent();
            MyDataInitializer dataInitializer = new MyDataInitializer();
            context = new CarsContext();
            carRepo = new CarRepository();
            cars = carRepo.GetTable();
            dataInitializer.InitializeDatabase(context);

            MessageBox.Show(context.Orders.Count().ToString());

            /*dtgCars.ItemsSource = context.Cars.Local.ToBindingList();
            dtgCreditRisks.ItemsSource = context.CreditRisks.Local.ToBindingList();
            dtgOrders.ItemsSource = context.Orders.Local.ToBindingList();
            dtgCustomers.ItemsSource = context.Customers.Local.ToBindingList();
            */
            
            Init();

        }
        private void FormatButtons()
        {
            var bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#5bc0de");
            btn2.Background = (Brush)bc.ConvertFrom("#5bc0de");
            btn3.Background = (Brush)bc.ConvertFrom("#5bc0de");
            btn1.Padding = new Thickness(6);
            btn2.Padding = new Thickness(6);
            btn3.Padding = new Thickness(6);

           

            btn1.Cursor = Cursors.Hand;
            btn2.Cursor = Cursors.Hand;
            btn3.Cursor = Cursors.Hand;
            ID.Background = (Brush)bc.ConvertFrom("#5bc0de");

        }
        private void Init()
        {
            InitListBoxes();
            FormatButtons();
        }

        private void InitListBoxes()
        {
            lbxColors.ItemsSource = cars.GetAllColors();
            lbxMakes.ItemsSource = cars.GetAllMakes();              //extension methods verwendet
            lbxColors2.ItemsSource = cars.GetAllColors();

            List<CarViewModel> carsVm = new List<CarViewModel>();
            foreach (Car c in carRepo.GetAll())
            {
                carsVm.Add(new CarViewModel(c));
            }
            lbxAllCars.ItemsSource = carsVm;
        }

        private void SearchByColor(object sender, SelectionChangedEventArgs e)
        {
            if (lbxColors.SelectedItem == null)
                return;

            List<Car> cars = carRepo.GetCarsByColor(lbxColors.SelectedItem.ToString());
            List<CarViewModel> carViewModels = new List<CarViewModel>();
            foreach (Car c in cars)
            {
                carViewModels.Add(new CarViewModel(c));
            }
            lbxCars.ItemsSource = carViewModels;
        }

        private void SearchByMake(object sender, SelectionChangedEventArgs e)
        {
            if (lbxMakes.SelectedItem == null)
                return;

            List<Car> cars = carRepo.GetCarsByMake(lbxMakes.SelectedItem.ToString());
            List<CarViewModel> carViewModels = new List<CarViewModel>();
            foreach (Car c in cars)
            {
                carViewModels.Add(new CarViewModel(c));
            }
            lbxCars2.ItemsSource = carViewModels;
        }

        private void CountByColor(object sender, SelectionChangedEventArgs e)
        {
            if (lbxColors2.SelectedItem == null)
                return;
            lblCount.Content = carRepo.CountCarsByColor(lbxColors2.SelectedItem.ToString());
        }

        private void CreateCar(object sender, RoutedEventArgs e)
        {
            if (txtAddedColor != null && txtAddedColor.Text != "" &&
                txtAddedMake != null && txtAddedMake.Text != "" &&
                txtAddedType != null && txtAddedType.Text != "")
            {
                int addedItems = carRepo.Add(new Car
                {
                    Make = txtAddedMake.Text,
                    Color = txtAddedColor.Text,
                    PetName = txtAddedType.Text
                });

                MessageBox.Show(addedItems.ToString() + " Items Added");
                txtAddedMake.Text = "";
                txtAddedColor.Text = "";
                txtAddedType.Text = "";
                Init();
            }
            else
            {
                MessageBox.Show("Alle Spalten befüllen!");
            }
        }

        private void UpdateCar(object sender, RoutedEventArgs e)
        {
            if ((CarViewModel)lbxAllCars.SelectedItem != null)
            {
                Car toUpdate = carRepo.GetOne(((CarViewModel)lbxAllCars.SelectedItem).CarID);
                
                toUpdate.Make = txtUpdatedMake.Text;
                toUpdate.Color = txtUpdatedColor.Text;
                toUpdate.PetName = txtUpdatedType.Text;

                int x = carRepo.Save();

                MessageBox.Show(x.ToString() + " items updated!");

                Init();
            }
        }

        private void lbxAllCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((CarViewModel)lbxAllCars.SelectedItem != null)
            {
                ID.Content = ((CarViewModel)lbxAllCars.SelectedItem).CarID;
                txtUpdatedColor.Text = ((CarViewModel)lbxAllCars.SelectedItem).Color;
                txtUpdatedMake.Text = ((CarViewModel)lbxAllCars.SelectedItem).Make;
                txtUpdatedType.Text = ((CarViewModel)lbxAllCars.SelectedItem).Type;

            }
        }

        private void DeleteCar(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Sind Sie sicher?", "Löschen", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;

            if ((CarViewModel)lbxAllCars.SelectedItem != null)
            {
                carRepo.Delete(carRepo.GetOne(Convert.ToInt32(ID.Content)));
                Init();
            }
        }
    }
}
