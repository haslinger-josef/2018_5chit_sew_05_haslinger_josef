﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181005_CRUD_WPF_CAR
{
    public abstract class GenericRepository<T> : IDisposable where T : class, new()
    {
        public CarsContext Context { get; } = new CarsContext();
        protected DbSet<T> Table;

        public T GetOne(int? id) => Table.Find(id);
        public List<T> GetAll() => Table.ToList();
        public int Add(T entity)
        {
            Table.Add(entity);
            return Context.SaveChanges();
        }
        public int AddRange(IList<T> entities)
        {
            foreach (T item in entities)
            {
                Table.Add(item);
            }
           return Context.SaveChanges();
        }

        public int Save()
        {
            return Context.SaveChanges();
        }

        public int AddRange(IEnumerable<T> entities)
        {
            foreach (T item in entities)
            {
                Table.Add(item);
            }
            return Context.SaveChanges();
        }
        public int Delete(T entity)
        {
            Table.Remove(entity);
            return Context.SaveChanges();
        }

        public List<T> ExecuteQuery(string sql) => Table.SqlQuery(sql).ToList();

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
