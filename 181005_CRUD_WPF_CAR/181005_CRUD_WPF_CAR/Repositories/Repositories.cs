﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181005_CRUD_WPF_CAR
{
    public class CarRepository : GenericRepository<Car>
    {
        public CarRepository()
        {
            Table = base.Context.Cars;
        }
        public DbSet<Car> GetTable()
        {
            return Table;
        }

        //public List<string> GetAllColors()
        //{
        //    List<string> x = (from t in Table
        //                      select t.Color).ToList();

        //    return x.Distinct().ToList();
        //}

        //public List<string> GetAllMakes()
        //{
        //    List<string> x = (from t in Table
        //                      select t.Make).ToList();

        //    return x.Distinct().ToList();
        //}

        public List<Car> GetCarsByColor(string color)
        {
            return Table.Where(x => x.Color == color).ToList();
        }
        public List<Car> GetCarsByMake(string make)
        {
            return Table.Where(x => x.Make == make).ToList();
        }
        public int CountCarsByColor(string color)
        {
            return Table.Where(x => x.Color == color).Count();
        }
    }

    static class MyExt
    {
       static public List<string> GetAllMakes(this DbSet<Car> Table)
        {
            List<string> x = (from t in Table
                              select t.Make).ToList();

            return x.Distinct().ToList();
        }
        static public List<string> GetAllColors(this DbSet<Car> Table)
        {
            List<string> x = (from t in Table
                              select t.Color).ToList();

            return x.Distinct().ToList();
        }

    }

    

    class CustomerRepository : GenericRepository<Customer>
    {
        public CustomerRepository()
        {
            Table = base.Context.Customers;
        }
        public List<Customer> GetByFirstname(string name)
        {
            return Table.Where(x=>x.FirstName == name).ToList();
        }
        public List<string> GetAllFullname()
        {
            return (from t in Table
                           select t.FirstName + t.LastName).ToList();       
        }

        public List<Customer> OrderByLastnameDescending()
        {
            return Table.OrderByDescending(x => x.LastName).ToList();
        }
    }
    class CreditRiskRepository : GenericRepository<CreditRisk>
    {
        public CreditRiskRepository()
        {
            Table = base.Context.CreditRisks;
        }
        public List<string> GetAllFullname()
        {
            return (from t in Table
                    select t.FirstName + t.LastName).ToList();
        }

        public List<CreditRisk> OrderByLastnameDescending()
        {
            return Table.OrderByDescending(x => x.LastName).ToList();
        }
        public List<CreditRisk> FindByLastname(string name)
        {
            return Table.Where(x => x.LastName==name).ToList();
        }
    }
    class OrderRepository : GenericRepository<Order>
    {
        public OrderRepository()
        {
            Table = base.Context.Orders;
        }
    }
}
