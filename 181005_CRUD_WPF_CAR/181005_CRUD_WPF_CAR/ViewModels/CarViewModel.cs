﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181005_CRUD_WPF_CAR
{
    class CarViewModel : INotifyPropertyChanged
    {
        private Car car;
        public CarViewModel(Car car)
        {
            this.car = car;
        }
        public int CarID
        {
            get { return car.CarId; }
            private set { }
        }
        public string Make
        {
            get { return car.Make; }
            set
            {
                car.Color = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Make"));
            }
        }
        public string Color
        {
            get { return car.Color; }
            set
            {
                car.Color = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Color"));
            }
        }
        public string Type
        {
            get { return car.PetName; }
            set
            {
                car.PetName = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Type"));
            }
        }

        public override string ToString()
        {
            return String.Format("ID: {0} Make: {1} Type: {2} Color: {3} ", CarID, Make, Type, Color);
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}









//public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();