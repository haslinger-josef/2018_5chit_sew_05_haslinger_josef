﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181005_CRUD_WPF_CAR.ViewModels
{
    class CarsViewModel
    {
        public BindingList<Car> Cars { get; set; }

        public CarsViewModel(CarsContext c)
        {
            Cars = c.Cars.Local.ToBindingList();
        }
    }
}
