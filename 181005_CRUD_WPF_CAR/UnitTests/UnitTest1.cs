﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using _181005_CRUD_WPF_CAR;
using System.Collections.Generic;
using System.Data.Entity;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {

        /*
         
       public List<Car> GetCarsByColor(string color)
        {
            return Table.Where(x => x.Color == color).ToList();
        }
        public List<Car> GetCarsByMake(string make)
        {
            return Table.Where(x => x.Make == make).ToList();
        }
        public int CountCarsByColor(string color)
        {
            return Table.Where(x => x.Color == color).Count();
        }
      */
        [TestMethod]
        public void GetCarsByColorTest()
        {
            CarRepository repo = new CarRepository();

            List<Car> cars = repo.GetCarsByColor("Black");

            foreach (Car item in cars)
            {
                Assert.AreEqual("Black", item.Color);
            }
        }

        [TestMethod]
        public void GetCarsByMakeTest()
        {
            CarRepository repo = new CarRepository();

            List<Car> cars = repo.GetCarsByMake("BMW");

            foreach (Car item in cars)
            {
                Assert.AreEqual("BMW", item.Make);
            }
        }

        [TestMethod]
        public void CountCarsByColorTest()
        {
            CarRepository repo = new CarRepository();

            int count = repo.CountCarsByColor("Black");


            Assert.AreEqual(4, count);
            
        }
    }
}
