﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Song
{
    public class Song
    {
        public Song() { }
        public string ArtistName { get; set; }
        public String SongTitle { get; set; }
    }
    public class SongViewModel :INotifyPropertyChanged
        {
        public Song Song { get; set; } 

        public SongViewModel()
        {
            Song = new Song
            {
                ArtistName = "Unknown",
                SongTitle = "Unknown"
            };
        }
        public string ArtistName
        {
            get { return Song.ArtistName; }
            set
            {
                if(Song.ArtistName != value)
                {
                    Song.ArtistName = value;
                    RaisePropertyChanged("ArtistName");
                }
            }
        }
        public String SongTitle
        {
            get { return Song.SongTitle; }
            set
            {
                if (Song.SongTitle != value)
                {
                    Song.SongTitle = value;
                    RaisePropertyChanged("SongTitle");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propname)
        {
            if(PropertyChanged !=null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propname));
            }
        }
    }
}
