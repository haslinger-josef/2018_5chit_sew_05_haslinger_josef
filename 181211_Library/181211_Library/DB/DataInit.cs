﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _181211_Library.Model;

namespace _181211_Library.DB
{
     class DataInit : DropCreateDatabaseIfModelChanges<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            var magazines = new List<AArticle>
            {
                new Magazine {Title = "Most successful actors of all time", Category="Celebrities"},
                new Magazine {Title = "How solve the plastic problem", Category="Environment"},
                new Magazine {Title = "Lions ... Kings of Africa", Category="Animals"}
            };


            var books = new List<Model.AArticle>
            {
                new Book {Title = "Steve Jobs ... Legend", Category="Biographies", Isbn="834-1-1212-8"},
                new Book {Title = "How technology changed everyones life", Category="Technology", Isbn="456-3-78978-4"},
                new Book {Title = "Costa Rica ... a beautiful place", Category="Nature", Isbn="234-1-23423-5"}
            };


            var authors = new List<APublisher>
            {
                new Author { Name="Steve Black", AArticles = { magazines[0] } },
                new Author { Name="Tony Stark", AArticles = { books[0] } },
                new Author { Name="Mike Wally", AArticles = { magazines[1] } }
            };


            var publishers = new List<APublisher>
            {
                new Publisher { Name="Forbes", AArticles = { books[1], magazines[2] } },
                new Publisher { Name="Gadget", AArticles = { magazines[2] } },
                new Publisher { Name="American Photo", AArticles = { books[2] } }
            };


            var addresses = new List<Address>
            {
                new Address { City = "New York", Street = "State Str", ZipCode = "234234" },
                new Address { City = "New York", Street = "Martin Luther King Str", ZipCode = "234423" },
                new Address { City = "St. Louis", Street = "Forest Str", ZipCode = "354354" }
            };

            var schools = new List<School>
            {
                new School { Name="Cristo Rey New York High School" },
                new School { Name="Stuyvesant High School" },
                new School { Name="Abraham Lincoln High School" }
            };

            var students = new List<Student>
            {
                new Student { Firstname = "Tony", Lastname = "Graham", ClassRoom = "9A", School = schools[0] },
                new Student { Firstname = "Marcus", Lastname = "Graham", ClassRoom = "9A", School = schools[0] },
                new Student { Firstname = "Ben", Lastname = "Stiller", ClassRoom = "10A", School = schools[1] },
                new Student { Firstname = "Ken", Lastname = "Todd", ClassRoom = "12B", School = schools[1] },
                new Student { Firstname = "Fill", Lastname = "Miller", ClassRoom = "12C", School = schools[2] },
                new Student { Firstname = "Michael", Lastname = "Star", ClassRoom = "10C", School = schools[2] }
            };

            var teachers = new List<Teacher>
            {
                new Teacher { Firstname = "Theresa", Lastname = "Miller", School = schools[0]},
                new Teacher { Firstname = "Thomas", Lastname = "Smith", School = schools[0] },
                new Teacher { Firstname = "Bianca", Lastname = "Stone", School = schools[1] },
                new Teacher { Firstname = "Benjamin", Lastname = "Gates", School = schools[1] },
                new Teacher { Firstname = "Walter", Lastname = "White", School = schools[2] },
                new Teacher { Firstname = "Louise", Lastname = "Smith", School = schools[2] }
            };

            var subjects = new List<Model.Subject>
            {
                new Subject { Name="Physics", Teacher=teachers[0] },
                new Subject { Name="Sports", Teacher=teachers[2] },
                new Subject { Name="Maths", Teacher=teachers[4] },
                new Subject { Name="Chemics", Teacher=teachers[0] },
                new Subject { Name="Biology", Teacher=teachers[1] },
                new Subject { Name="Geography", Teacher=teachers[1] },
                new Subject { Name="History", Teacher=teachers[5] }
            };

            var normalBuyer = new List<NormalBuyer>
            {
                new NormalBuyer { Firstname = "Simon", Lastname = "Mosby", Address = addresses[0] },
                new NormalBuyer { Firstname = "Barney", Lastname = "Stinson", Address = addresses[2] }
            };

            var orders = new List<Order>
            {
                new Order { Date=DateTime.Parse("2018-07-04"), ABuyer = normalBuyer[0], AArticle = magazines[0] },
                new Order { Date=DateTime.Parse("2018-07-04"), ABuyer = normalBuyer[0], AArticle = magazines[1] },
                new Order { Date=DateTime.Parse("2018-07-04"), ABuyer = normalBuyer[0], AArticle = books[0] },
                new Order { Date=DateTime.Parse("2018-08-13"), ABuyer = normalBuyer[1], AArticle = magazines[0] },
                new Order { Date=DateTime.Parse("2018-08-13"), ABuyer = normalBuyer[1], AArticle = magazines[1] },
                new Order { Date=DateTime.Parse("2018-08-13"), ABuyer = normalBuyer[1], AArticle = books[2] },
                new Order { Date=DateTime.Parse("2017-06-15"), ABuyer = teachers[2], AArticle = magazines[0] },
                new Order { Date=DateTime.Parse("2016-07-04"), ABuyer = teachers[2], AArticle = books[0] },
                new Order { Date=DateTime.Parse("2017-06-15"), ABuyer = teachers[3], AArticle = magazines[1] },
                new Order { Date=DateTime.Parse("2016-07-04"), ABuyer = teachers[3], AArticle = magazines[2] },
                new Order { Date=DateTime.Parse("2017-06-15"), ABuyer = teachers[4], AArticle = books[0] },
                new Order { Date=DateTime.Parse("2016-07-04"), ABuyer = teachers[4], AArticle = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), ABuyer = students[3], AArticle = magazines[0] },
                new Order { Date=DateTime.Parse("2018-05-24"), ABuyer = students[3], AArticle = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), ABuyer = students[2], AArticle = magazines[0] },
                new Order { Date=DateTime.Parse("2018-05-24"), ABuyer = students[1], AArticle = books[1] },
                new Order { Date=DateTime.Parse("2017-10-09"), ABuyer = students[0], AArticle = magazines[2] },
                new Order { Date=DateTime.Parse("2018-05-24"), ABuyer = students[5], AArticle = books[0] }
            };

            magazines.ForEach(x => context.Articles.Add(x));
            books.ForEach(x => context.Articles.Add(x));
            authors.ForEach(x => context.Publishers.Add(x));
            publishers.ForEach(x => context.Publishers.Add(x));
            addresses.ForEach(x => context.Addresses.Add(x));
            students.ForEach(x => context.Buyers.Add(x));
            subjects.ForEach(x => context.Subjects.Add(x));
            teachers.ForEach(x => context.Buyers.Add(x));
            normalBuyer.ForEach(x => context.Buyers.Add(x));
            schools.ForEach(x => context.Schools.Add(x));
            orders.ForEach(x => context.Orders.Add(x));

            context.SaveChanges();

            base.Seed(context);
        }
    }
}
