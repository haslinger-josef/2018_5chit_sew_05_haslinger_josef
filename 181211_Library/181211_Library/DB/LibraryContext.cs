﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _181211_Library.Model;

namespace _181211_Library.DB
{
    public class LibraryContext : DbContext
    {
        private static LibraryContext instance = new LibraryContext();

        public static LibraryContext GetInstance()
        {
            return instance;
        }
        private LibraryContext() : base("LibraryDatabase")
        {
            Database.Delete();
            Database.SetInitializer(new DataInit());
            Database.Initialize(true);
        }
        public DbSet<AArticle> Articles { get; set; }
        public DbSet<ABuyer> Buyers { get; set; }
        public DbSet<APublisher> Publishers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<School> Schools { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<APublisher>()
                .Map<Author>(p => p.Requires("PublisherType").HasValue("Author"))
                .Map<Publisher>(p => p.Requires("PublisherType").HasValue("Publisher"));
            modelBuilder.Entity<APublisher>()
                .HasMany(p => p.AArticles)
                .WithMany(p => p.APublishers)
                .Map(tc =>
                {
                    tc.MapLeftKey("Article_Id");
                    tc.MapRightKey("Publisher_Id");
                    tc.ToTable("Articles_Publishers");
                });


            modelBuilder.Entity<AArticle>()
                .Map<Book>(a => a.Requires("ArticleType").HasValue("Book"))
                .Map<Magazine>(a => a.Requires("ArticleType").HasValue("Magazine"));

            modelBuilder.Entity<AArticle>()
                .HasMany(a => a.Orders)
                .WithRequired(a => a.AArticle)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<ABuyer>()
                .Map<NormalBuyer>(b => b.Requires("BuyerType").HasValue("NormalBuyer"))
                .Map<Teacher>(b => b.Requires("BuyerType").HasValue("Teacher"))
                .Map<Student>(b => b.Requires("BuyerType").HasValue("Student"));

            modelBuilder.Entity<ABuyer>()
                .HasMany(b => b.Orders)
                .WithRequired(b => b.ABuyer)
                .WillCascadeOnDelete(false);

            //NormalBuyer
            modelBuilder.Entity<Address>()
                .HasOptional(a => a.NormalBuyer)
                .WithOptionalPrincipal(a => a.Address);

            //School
            modelBuilder.Entity<School>()
                .HasMany(s => s.Students)
                .WithRequired(s => s.School)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<School>()
                .HasMany(s => s.Teachers)
                .WithRequired(s => s.School)
                .WillCascadeOnDelete(false);

            //Teacher
            modelBuilder.Entity<Teacher>()
                .HasMany(t => t.Subjects)
                .WithRequired(t => t.Teacher)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);

            // Danke Simon :)
        }
    }
}

