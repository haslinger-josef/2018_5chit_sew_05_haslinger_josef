﻿using _181211_Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _181211_Library.DB;
using _181211_Library.ViewModels;

namespace _181211_Library
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LibraryContext context = LibraryContext.GetInstance();
        }

        private void LbxArticlesSelectionChangend(object sender, SelectionChangedEventArgs e)
        {
            stpArticle.DataContext = (ArticleVM)lbxArticles.SelectedItem;
        }
    }
}
