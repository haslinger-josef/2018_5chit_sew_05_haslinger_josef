﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public abstract class AArticle
    {
        [Key]
        public int? Article_Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public virtual List<Order> Orders { get; set; } = new List<Order>();
        public virtual List<APublisher> APublishers { get; set; } = new List<APublisher>();

        public override string ToString()
        {
            return Title + " " + Category;
        }
    }
}
