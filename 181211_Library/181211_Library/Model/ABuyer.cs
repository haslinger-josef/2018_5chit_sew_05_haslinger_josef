﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public abstract class ABuyer
    {
        [Key]
        public int? Buyer_Id { get; set; }

        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public virtual List<Order> Orders { get; set; } = new List<Order>();

        public override string ToString()
        {
            return Firstname + " " + Lastname;
        }
    }
}
