﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public abstract class APublisher
    {
        [Key]
        public int? Publisher_Id { get; set; }
        public string Name { get; set; }
        public virtual List<AArticle> AArticles { get; set; } = new List<AArticle>();
    }
}
