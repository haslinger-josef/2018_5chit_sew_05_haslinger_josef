﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace _181211_Library.Model
{
    public class Address
    {
        [Key]
        public int? Address_Id { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string ZipCode { get; set; }

        public virtual NormalBuyer NormalBuyer { get; set; }
    }
}
