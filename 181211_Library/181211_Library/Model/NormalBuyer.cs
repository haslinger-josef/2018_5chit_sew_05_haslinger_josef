﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public class NormalBuyer : ABuyer
    {
        public virtual Address Address { get; set; }
    }
}
