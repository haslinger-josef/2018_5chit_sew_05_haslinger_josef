﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public class Order
    {
        [Key]
        public int? Order_Id { get; set; }

        public DateTime Date { get; set; }

        public virtual ABuyer ABuyer { get; set; }

        public virtual AArticle AArticle { get; set; }
    }
}
