﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public class School
    {
        [Key]
        public int? School_Id { get; set; }

        public string Name { get; set; }

        public virtual List<Teacher> Teachers { get; set; } = new List<Teacher>();
        public virtual List<Student> Students { get; set; } = new List<Student>();
    }
}
