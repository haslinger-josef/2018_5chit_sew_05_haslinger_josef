﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public class Subject
    {
        [Key]
        public int? Subject_Id { get; set; }

        public string Name { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}
