﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _181211_Library.Model
{
    public class Teacher : ABuyer
    {
        public virtual School School { get; set; }

        public virtual List<Subject> Subjects { get; set; } = new List<Subject>();
    }
}
