﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _181211_Library.DB;
using _181211_Library.Model;

namespace _181211_Library.Repository
{
    public abstract class ARepository<T> where T:class
    {
        protected LibraryContext context = LibraryContext.GetInstance();

        protected DbSet<T> Table;

        public T GetOne(int id)
        {
            return Table.Find(id);
        }
        public List<T> GetAll()
        {
            return Table.ToList();
        }
        public int Add(T item)
        {
            Table.Add(item);
            return context.SaveChanges();
        }
        public int AddRange(List<T> items)
        {
            Table.AddRange(items);
            return context.SaveChanges();
        }
        public int Delete(T entity)
        {
            Table.Remove(entity);
            return context.SaveChanges();
        }

        public List<T> ExecuteQuery(string sql) => Table.SqlQuery(sql).ToList();


    }
}
