﻿using _181211_Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181211_Library.Repository
{
    public class BuyerRepository : ARepository<ABuyer>
    {
        public BuyerRepository()
        {
            Table = base.context.Buyers;
        }
        public List<Student> GetAllStudents()
        {
            return Table.OfType<Student>().ToList();
        }
        public List<Teacher> GetAllTeachers()
        {
            return Table.OfType<Teacher>().ToList();
        }
        public List<NormalBuyer> GetAllNormalBuyers()
        {
            return Table.OfType<NormalBuyer>().ToList();
        }
        public List<ABuyer> GetBuyerByFirstName(string firstname)
        {
            return Table.Where(x => x.Firstname == firstname).ToList();
        }
        public List<ABuyer> GetBuyerByLastName(string lastname)
        {
            return Table.Where(x => x.Lastname == lastname).ToList();
        }
    }
}
