﻿using _181211_Library.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using _181211_Library.Command;
using _181211_Library.DB;

namespace _181211_Library.ViewModels
{
    class ArticleVM : AViewModel
    {
        private AArticle article;
        public ArticleVM(AArticle article)
        {
            this.article = article;
        }

        public string Title
        {
            get
            {
                return article.Title;
            }
            set
            {
                if (value != article.Title)
                {
                    article.Title = value;
                    OnPropertyChangend("Title");
                }
            }
        }
        public string Category
        {
            get
            {
                return article.Category;
            }
            set
            {
                if (value != article.Category)
                {
                    article.Category = value;
                    OnPropertyChangend("Category");
                }
            }
        }
        public ICommand UpdateArticle => new CustomCommand(Update);
        private void Update()
        {
            LibraryContext.GetInstance().SaveChanges();
        }
        public override string ToString()
        {
            return Title +" ("+Category+")";
        }
    }
}
