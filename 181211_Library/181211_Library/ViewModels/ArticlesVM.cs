﻿using _181211_Library.DB;
using _181211_Library.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181211_Library.ViewModels
{
    class ArticlesVM 
    {
        private LibraryContext context = LibraryContext.GetInstance();

        private BindingList<ArticleVM> articles = new BindingList<ArticleVM>();

        public BindingList<ArticleVM> Articles
        {
            get { return articles; }
            set { articles = value; }
        }

        public ArticlesVM()
        {
            foreach (AArticle item in context.Articles)
            {
                articles.Add(new ArticleVM(item));
            }
        }
    }
}
