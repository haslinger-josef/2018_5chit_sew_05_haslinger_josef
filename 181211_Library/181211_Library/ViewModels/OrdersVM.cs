﻿using _181211_Library.DB;
using _181211_Library.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _181211_Library.Repository
{
    public class OrdersVM
    {
        public BindingList<Order> Orders { get; }

        public OrdersVM()
        {
            LibraryContext context = LibraryContext.GetInstance();
            context.Orders.Load();
            Orders = context.Orders.Local.ToBindingList();
        }
    }
}
