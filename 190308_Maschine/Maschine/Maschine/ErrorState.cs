﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maschine
{
    public class ErrorState : IMachineState
    {
        public IMachineState Maintain()
        {
            Logger.GetInstance().AddMessage("ERROR  ==> MAINTAIN");
;            return this;
        }

        public IMachineState Process()
        {
            Logger.GetInstance().AddMessage("ERROR  ==> PROCESS");
            return this;
        }

        public IMachineState SwitchOn()
        {
            Logger.GetInstance().AddMessage("ERROR  ==> SWITCHON");
            return new InitializedState();
        }
        public override string ToString()
        {
            return "ErrorState";
        }
    }
}
