﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maschine
{
    public class InitializedState : IMachineState
    {
        public IMachineState Maintain()
        {
            Logger.GetInstance().AddMessage("INIT  ==> MAINTAIN");
            return new ErrorState();
        }

        public IMachineState Process()
        {
            Logger.GetInstance().AddMessage("INIT  ==> PROCESS");
            return new WorkingState();
        }

        public IMachineState SwitchOn()
        {
            Logger.GetInstance().AddMessage("INIT  ==> SWITCHON");
            return this;
        }
        public override string ToString()
        {
            return "InitState";
        }
    }
}
