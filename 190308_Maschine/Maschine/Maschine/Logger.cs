﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Maschine
{
    public class Logger
    {
        private static readonly Logger _singleton = new Logger();
        public static Logger GetInstance()
        {
            return _singleton;
        }
        private Logger() { }

        public ObservableCollection<string> messages = new ObservableCollection<string>();
        public ObservableCollection<string> Messages { get => messages; private set { } }
        public void AddMessage(string mess)
        {
            messages.Add(mess);
            XDocument logfile = XDocument.Load("logfile.xml");
            XElement root = logfile.Element("log");
            XElement logentry = new XElement("logentry");
            logentry.Add(new XElement("message", mess));
            logentry.Add(new XElement("date"), DateTime.Now);
            
            root.Add(logentry);
            logfile.Save("logfile.xml");
        }
    }
}
