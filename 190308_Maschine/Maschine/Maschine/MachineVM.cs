﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Maschine
{
   public  class MachineVM : INotifyPropertyChanged
    {
        public MachineVM()
        {
            State = new InitializedState();
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<string> Log { get { return Logger.GetInstance().Messages; } }
        public void CallPropertyChanged([CallerMemberName] string prop = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public IMachineState State
        {
            get { return state; }
            set
            {
                CallPropertyChanged();

                this.state = value;
            }
        }
        private IMachineState state;

        public RelayCommand SwitchOnCommand
        {
            get
            {
                return new RelayCommand(
                        o => this.State = State.SwitchOn(),
                        o => true);
            }
        }
        public RelayCommand MaintainCommand
        {
            get
            {
                return new RelayCommand(
                        o => this.State = State.Maintain(),
                        o => true);
            }
        }
        public RelayCommand ProcessCommand
        {
            get
            {
                return new RelayCommand(
                        o => this.State = State.Process(),
                        o => true);
            }
        }

    }
}
