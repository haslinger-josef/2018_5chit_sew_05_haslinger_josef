﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maschine
{
    public class WorkingState : IMachineState
    {
        public IMachineState Maintain()
        {
            Logger.GetInstance().AddMessage("WORKING  ==> MAINTAIN");
            return new ErrorState();
        }
        public IMachineState Process()
        {
            Logger.GetInstance().AddMessage("WORKING  ==> PROCESS");

            return this;
        }

        public IMachineState SwitchOn()
        {
            Logger.GetInstance().AddMessage("WORKING  ==> SWITCHON");

            return this;
        }
        public override string ToString()
        {
            return "WorkingState";
        }
    }
}
